**********************************
MouseTrap - Raspberry Pi component
**********************************

This project contains the code of the Raspberry Pi part of MouseTrap project. It was developed in Python 2.7 and it has the form of a python module. This module communicates with Arduino, MQTT broker and Photo Storage API. Its main purpose is to take and upload the photos and make the bridge between Arduino and the MQTT broker.

|

============
Requirements
============

If you want to communicate securely with the MQTT broker, you need to get a certificate to communicate with it. For that, run the following command from the project root directory:

.. code-block:: bash

    echo -n | openssl s_client -connect localhost:8443 | sed -ne '/-BEGIN CERTIFICATE-/,/-END CERTIFICATE-/p' > certs/mqtt-broker.pem

Also, add a .env file with the following

.. code-block::

    AUTH0_CLIENT_ID=nxXq9GaUdUnaWEF0YhwBoaAmBy5ABRmj
    AUTH0_DOMAIN=mousetrap.eu.auth0.com
    AUTH0_CONNECTION=Traps

Then, you are able to run it as follows

.. code-block:: bash

    python rpi_trap

|

===============
Issue Reporting
===============

If you have found a bug or if you have a feature request, please report them at this repository issues section. Please do not report security vulnerabilities on the public issues section, instead send an email to up201200272 [at] fc.up.pt

|

======
Author
======

`José Sousa
<#>`_ &
`José C. Paiva
<#>`_

|

=======
License
=======

This project is licensed under the MIT license. See the `LICENSE
<LICENSE.txt>`_ file for more info.