# coding=utf-8

import os
from setuptools import setup


def read(fname):
    return open(os.path.join(os.path.dirname(__file__), fname)).read()


setup(
        name="rpi_trap",
        version="1.0.0",
        author="José C. Paiva",
        author_email="josepaiva94@gmail.com",
        description=("Implementation of the Raspberry Pi component of the MouseTrap"),
        license="MIT",
        keywords="MouseTrap IoT raspberry",
        url="",
        packages=['rpi_trap'],
        long_description=read('README.rst'),
        install_requires=[
            'dotenv>=0.0.5',
            'requests>=2.13.0',
            'paho-mqtt>=1.2',
            'schedule>=0.4.2'
        ],
        classifiers=[
            "Development Status :: 3 - Alpha",
            "Programming Language :: Python",
            "Programming Language :: Python :: 2",
            "Programming Language :: Python :: 2.7",
            "Programming Language :: Python :: 3",
            "Programming Language :: Python :: 3.0",
            "Programming Language :: Python :: 3.1",
            "Programming Language :: Python :: 3.2",
            "Programming Language :: Python :: 3.3",
            "Programming Language :: Python :: 3.4",
            "Programming Language :: Python :: 3.5",
            "Topic :: Home Automation",
            "License :: OSI Approved :: MIT License"
        ],
        entry_points={
            'console_scripts': [
                'rpi_trap = rpi_trap.__main__:main'
            ]
        },
)
