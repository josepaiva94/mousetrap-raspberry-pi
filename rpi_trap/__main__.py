# coding=utf-8

import time
import serial
import serial.tools.list_ports
import schedule

import settings
from mqtt.mqtt_client import *
from utils.auth import *
from utils.photo_manager import *

__author__ = 'josepaiva'
__copyright__ = 'Copyright (C) 2017 José Carlos Paiva'
__license__ = 'MIT'
__version__ = '1.0.0'

ARD_LOCK=0

# login credentials
email = Auth.get_random_email()
password = Auth.get_random_password()

# auth object
auth = Auth(settings.AUTH0_CLIENT_ID, settings.AUTH0_DOMAIN, settings.AUTH0_CONNECTION)

# user data
user_id = ""
access_token = ""
id_token = ""

# MQTT Client
mqttc = None


# Functions that deal with authentication

def signup():
    global user_id
    res = auth.signup(email, password, None)
    user_id = res.get('_id')


def login():
    global id_token, access_token
    res = auth.login(email, password, None)
    id_token = res.get(ID_TOKEN_KEY)
    access_token = res.get(ACCESS_TOKEN_KEY)


def select_topics():
    auth.patch_metadata('auth0|' + user_id, id_token, user_metadata={
        'topics': ':'.join([TRAP_DOOR_TOPIC % user_id,
                            TRAP_DOOR_STATE_TOPIC % user_id,
                            TRAP_PICTURE_TOPIC % user_id,
                            TRAP_PICTURE_REQUEST_TOPIC % user_id,
                            TRAP_TIMEOUT_TOPIC % user_id,
                            TRAP_TIMEOUT_ACK_TOPIC % user_id,
                            TRAP_ALERT_TOPIC % user_id])
    })


def renew_token():
    global mqttc, id_token

    login()

    if mqttc is not None:
        mqttc.pass_or_id_token = id_token


# Functions to handle MQTT messages received

def on_door_topic_message(state):
    arduino.write(bytes(state)+b'\r\n')
    door_state=int(state)
    print 'Received message about "door" with state %d' % state
    mqttc.publish(PublishTopic.DOOR_STATE, door_state)


def on_picture_request_message():
    global id_token, mqttc
    # upload a photo
    pm = PhotoManager(id_token)
    photo_id = pm.take_and_upload()
    print 'photo_id=',photo_id
    mqttc.publish(PublishTopic.PICTURE, photo_id)


def on_trap_timeout_message(time):
    global timeout_dur
    delta = time - timeout_dur
    if delta > 0:
        for _ in range(delta):
            arduino.write(b'4\r\n')
    elif delta < 0:
        for _ in range(-delta):
            arduino.write(b'3\r\n')
    print 'Received message about "timeout" with time %d' % time


if __name__ == '__main__':

    print 'EMAIL: ' + email
    print 'PASSWORD: ' + password

    signup()  # signup rpi in first time

    login()  # login rpi

    select_topics()  # select own trap's topics

    renew_token()  # renew token to get topics

    # renew token auth0 every 8 hours
    schedule.every(8).hours.do(renew_token)

    print 'USER ID: ' + user_id
    print 'ACCESS TOKEN: ' + access_token
    print 'ID TOKEN: ' + id_token

    # MQTT client setup
    mqttc = MqttClient(user_id, id_token)
    mqttc.on_door_topic_message = on_door_topic_message
    mqttc.on_picture_request_message = on_picture_request_message
    mqttc.on_trap_timeout_message = on_trap_timeout_message
    mqttc.connect()

    mqttc.loop_start()

    # Serial setup
    for port in serial.tools.list_ports.comports():
	serial_device=port[0]
        break
    else:
        raise Exception('No Arduino detected!')
        exit(1)

    arduino = serial.Serial(serial_device,9600,timeout=0)
    # make sure arduino is ready
    _ = arduino.readline()
    door_state=1
    timeout_dur=5
    mqttc.publish(PublishTopic.DOOR_STATE, door_state)
    mqttc.publish(PublishTopic.TIMEOUT_ACK, timeout_dur)
    while True:
        try:
            ard=arduino.readline().strip()
            if ard != '':
                ard=int(ard)
                if ard in [0,1]:
                    door_state=ard
                    mqttc.publish(PublishTopic.DOOR_STATE, door_state)
                elif ard==2:  # mouse detected
                    on_picture_request_message()
                else:  # timeout
                    timeout_dur=int(ard)/1000
                    mqttc.publish(PublishTopic.TIMEOUT_ACK, timeout_dur)
        except:
            pass
        schedule.run_pending()

