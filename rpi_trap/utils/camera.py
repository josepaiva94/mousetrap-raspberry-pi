from picamera import PiCamera
from PIL import Image
from io import BytesIO
from time import sleep

cam=PiCamera(resolution=(320,240))

def take_picture(out):
	cam.capture(out,'jpeg')
	return out

def get_picture(file=None):
	if file is None:
		stream=BytesIO()
		file=take_picture(stream)
		file.seek(0)
	else:
		take_picture(file)
	return Image.open(file)

def preview(n):
	cam.start_preview()
	sleep(n)
	cam.stop_preview()
