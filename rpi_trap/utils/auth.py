# coding=utf-8

import string
from random import randint, choice

from constants import *
from base import *

__author__ = 'josepaiva'
__copyright__ = 'Copyright (C) 2017 José Carlos Paiva'
__license__ = 'MIT'
__version__ = '1.0.0'


class Auth(Base):
    """ Class that deals with signup, authentication & authorization
        Based on auth0-python package
    """

    def __init__(self, client_id, domain, connection):
        self.client_id = client_id
        self.domain = domain
        self.connection = connection

    def signup(self, email, password, user_metadata):
        """ Signup to Auth0 (using username and password)
            Based on auth0-python package
        """

        return self.post(
                DB_SIGNUP_URL % self.domain,
                data={
                    CLIENT_ID_KEY: self.client_id,
                    EMAIL_KEY: email,
                    PASSWORD_KEY: password,
                    CONNECTION_KEY: self.connection,
                    USER_METADATA_KEY: user_metadata
                },
                headers={CONTENT_TYPE_KEY: MIME_JSON_KEY}
        )

    def login(self, username, password, id_token, grant_type='password', device=None, scope='openid profile'):
        """ Login to Auth0 (using username and password)
            Based on auth0-python package
        """

        return self.post(
                OAUTH_RO_URL % self.domain,
                data={
                    CLIENT_ID_KEY: self.client_id,
                    USERNAME_KEY: username,
                    PASSWORD_KEY: password,
                    ID_TOKEN_KEY: id_token,
                    CONNECTION_KEY: self.connection,
                    DEVICE_KEY: device,
                    GRANT_TYPE_KEY: grant_type,
                    SCOPE_KEY: scope
                },
                headers={CONTENT_TYPE_KEY: MIME_JSON_KEY}
        )

    def patch_metadata(self, user_id, id_token, app_metadata=None, user_metadata=None):
        """ Update app_metadata and user_metadata fields
        """

        return self.patch(
                PATCH_USER_METADATA_URL % (self.domain, user_id),
                data={
                    #APP_METADATA_KEY: app_metadata,
                    USER_METADATA_KEY: user_metadata
                },
                headers={
                    AUTHORIZATION_KEY: AUTHORIZATION_BEARER % id_token,
                    CONTENT_TYPE_KEY: MIME_JSON_KEY
                }
        )

    @staticmethod
    def get_random_username():
        n = randint(4, 16)
        return ''.join(choice(string.ascii_lowercase + string.digits) for _ in range(n))

    @staticmethod
    def get_random_email():
        n = randint(5, 20)
        name = ''.join(choice(string.ascii_lowercase + string.digits) for _ in range(n))
        return name + '@mousetrap.com'

    @staticmethod
    def get_random_password():
        n = randint(5, 20)
        password = ''.join(choice(string.ascii_lowercase + string.digits +
                                  string.punctuation) for _ in range(n))
        return password

