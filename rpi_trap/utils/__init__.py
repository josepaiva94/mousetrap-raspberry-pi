# coding=utf-8

from auth import *
from base import *
from photo_manager import *

__author__ = 'josepaiva'
__copyright__ = 'Copyright (C) 2017 José Carlos Paiva'
__license__ = 'MIT'
__version__ = '1.0.0'


__all__ = ['auth', 'base', 'photo_manager']