# coding=utf-8

import json
import requests

__author__ = 'josepaiva'
__copyright__ = 'Copyright (C) 2017 José Carlos Paiva'
__license__ = 'MIT'
__version__ = '1.0.0'


class Base:
    """ Base class for making requests to Auth0
        Based on auth0-python package
    """

    def get(self, url, params={}, headers={}, **kwargs):
        return requests.get(url=url, params=params, headers=headers,  **kwargs).text

    def post(self, url, data={}, headers={}, **kwargs):
        response = requests.post(url=url, data=json.dumps(data),
                                 headers=headers, **kwargs)
        return self._process_response(response)

    def patch(self, url, data={}, headers={}, **kwargs):
        response = requests.patch(url=url, data=json.dumps(data),
                                  headers=headers, **kwargs)
        return self._process_response(response)

    @staticmethod
    def _process_response(response):
        text = json.loads(response.text) if response.text else {}

        if 'error' in text:
            raise Base.Auth0Error(status_code=404,
                                  error_code='Not Found',
                                  message=text['error'])
        elif 'statusCode' in text:
            raise Base.Auth0Error(status_code=text['statusCode'],
                                  error_code=text['code'],
                                  message=text['description'] if 'description' in text else '')

        return text

    class Auth0Error(Exception):
        """ Class that represents an error object from Auth0
        """

        def __init__(self, status_code, error_code, message):
            self.status_code = status_code
            self.error_code = error_code
            self.message = message

        def __str__(self):
            return '%s: %s' % (self.status_code, self.message)
