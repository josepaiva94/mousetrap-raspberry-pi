# coding=utf-8

""" Photo manager to take and upload photos
"""

import json
import requests
from os import remove as rmfile
import uuid

from constants import *
from camera import *


__author__ = 'josepaiva'
__copyright__ = 'Copyright (C) 2017 José Carlos Paiva'
__license__ = 'MIT'
__version__ = '1.0.0'


class PhotoManager():
    """ Class of a manager of photos
    """

    def __init__(self, id_token):
        self.id_token = id_token

    def take(self):
        """
        Take a photo
        :return: photo file name
        """
        self.photo_file_name=str(uuid.uuid4()).split('-')[0]+'.jpeg'
        photo_file_path=path.join(PHOTO_FOLDER_PATH,self.photo_file_name)
        take_picture(photo_file_path)

        return self.photo_file_name

    def upload(self, path):
        """
        Upload a photo to the storage server
        :param path: path to the photo
        :return: id of the photo on the server
        """

        files = {PHOTO_UPLOAD_FIELD: (self.photo_file_name, open(path, 'rb'), MIME_PHOTO_KEY)}
        res = requests.post(PHOTO_UPLOAD_URL, headers={
            AUTHORIZATION_KEY: AUTHORIZATION_BEARER % self.id_token
        }, files=files)
        res = json.loads(res.text) if res.text else {}
        rmfile(path)
        
        return res.get('id')

    def take_and_upload(self):
        """
        Take and upload photo
        :return: name of the resource in the storage server
        """

        photo_path = path.join(PHOTO_FOLDER_PATH, self.take())
        return self.upload(photo_path)


