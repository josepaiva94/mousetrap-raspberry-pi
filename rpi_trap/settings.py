# coding=utf-8

""" Settings file for MouseTrap's rpi_trap project
"""

import os
from os.path import join, dirname
from dotenv import Dotenv

import constants

__author__ = 'josepaiva'
__copyright__ = 'Copyright (C) 2017 José Carlos Paiva'
__license__ = 'MIT'
__version__ = '1.0.0'


# Load Env variables
env = None

try:
    dotenv_path = join(dirname(__file__), '.env')
    env = Dotenv(dotenv_path)
except IOError:
    env = os.environ

AUTH0_CLIENT_ID = env[constants.AUTH0_CLIENT_ID]
AUTH0_DOMAIN = env[constants.AUTH0_DOMAIN]
AUTH0_CONNECTION = env[constants.AUTH0_CONNECTION]
