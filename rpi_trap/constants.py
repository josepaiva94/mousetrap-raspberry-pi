# coding=utf-8

""" Constants file for MouseTrap's rpi_trap project
"""

from os import path

__author__ = 'josepaiva'
__copyright__ = 'Copyright (C) 2017 José Carlos Paiva'
__license__ = 'MIT'
__version__ = '1.0.0'


AUTH0_CLIENT_ID = 'AUTH0_CLIENT_ID'
AUTH0_CLIENT_SECRET = 'AUTH0_CLIENT_SECRET'
AUTH0_DOMAIN = 'AUTH0_DOMAIN'
AUTH0_CONNECTION = 'AUTH0_CONNECTION'
DB_SIGNUP_URL = 'https://%s/dbconnections/signup'
OAUTH_RO_URL = 'https://%s/oauth/ro'
PATCH_USER_METADATA_URL = 'https://%s/api/v2/users/%s'
AUTHORIZATION_KEY = 'authorization'
AUTHORIZATION_BEARER = 'Bearer %s'
APP_METADATA_KEY = 'app_metadata'
ACCESS_TOKEN_KEY = 'access_token'
CLIENT_ID_KEY = 'client_id'
CLIENT_SECRET_KEY = 'client_secret'
CONNECTION_KEY = 'connection'
CONTENT_TYPE_KEY = 'content-type'
DEVICE_KEY = 'device'
EMAIL_KEY = 'email'
GRANT_TYPE_KEY = 'grant_type'
ID_TOKEN_KEY = 'id_token'
MIME_JSON_KEY = 'application/json'
MIME_PHOTO_KEY = 'image/jpeg'
PASSWORD_KEY = 'password'
SCOPE_KEY = 'scope'
TOPICS_KEY = 'topics'
USER_METADATA_KEY = 'user_metadata'
USERNAME_KEY = 'username'
ROOT_DIR = path.dirname(path.abspath(__file__))
PHOTO_FOLDER_PATH = path.join(ROOT_DIR, '..', 'photos')
PHOTO_FILE_NAME = 'photo.jpg'
PHOTO_UPLOAD_URL = 'http://codingbooth.dynip.sapo.pt:3001/api/photos'
PHOTO_UPLOAD_FIELD = 'photo'
CERT_FOLDER_PATH = path.join(ROOT_DIR, '..', 'certs')
CERT_BROKER_PATH = path.join(CERT_FOLDER_PATH, 'mqtt-broker.pem')
BROKER_HOSTNAME = 'codingbooth.dynip.sapo.pt'
BROKER_PORT = '8443'
TRAP_PREFIX_TOPIC = 'traps/%s/'
TRAP_DOOR_TOPIC = TRAP_PREFIX_TOPIC + 'door' # open/close door
TRAP_DOOR_STATE_TOPIC = TRAP_PREFIX_TOPIC + 'door/state' # door IS open/closed
TRAP_PICTURE_TOPIC = TRAP_PREFIX_TOPIC + 'picture' # answer to a picture request
TRAP_PICTURE_REQUEST_TOPIC = TRAP_PREFIX_TOPIC + 'picture/request' # request a picture
TRAP_ALERT_TOPIC = TRAP_PREFIX_TOPIC + 'alert' # alert of mouse inside
TRAP_TIMEOUT_TOPIC = TRAP_PREFIX_TOPIC + 'timeout' # time for user to take action
TRAP_TIMEOUT_ACK_TOPIC = TRAP_PREFIX_TOPIC + 'timeout/ack' # inform that time for user to take action has changed


