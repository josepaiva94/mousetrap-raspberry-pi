# coding=utf-8

import paho.mqtt.client as mqtt

from constants import *

__author__ = 'josepaiva'
__copyright__ = 'Copyright (C) 2017 José Carlos Paiva'
__license__ = 'MIT'
__version__ = '1.0.0'


class MqttClient:
    """ Class that makes the communication with MQTT Broker """

    def __init__(self, user_id, pass_or_id_token):
        self.user_id = user_id
        self.pass_or_id_token = pass_or_id_token
        self.client = mqtt.Client(client_id=user_id)
        self.client.username_pw_set('JWT', pass_or_id_token)

    def connect(self):
        # disabled tls
        # self.client.tls_set(CERT_BROKER_PATH)
        # self.client.tls_insecure_set(True) # prevents error - ssl.SSLError: Certificate subject does not match remote hostname.
        self.client.on_connect = self.on_connect
        self.client.on_disconnect = self.on_disconnect
        self.client.on_message = self.on_message
        self.client.connect(BROKER_HOSTNAME, BROKER_PORT)

    def loop_forever(self):
        self.client.loop_forever()

    def loop_start(self):
        self.client.loop_start()

    def loop_stop(self, force):
        self.client.loop_stop(force)

    def on_connect(self, client, data, flags, rc):
        print 'connected...rc=%s' % str(rc)
        self.client.subscribe(TRAP_DOOR_TOPIC % self.user_id, qos=1)
        self.client.subscribe(TRAP_PICTURE_REQUEST_TOPIC % self.user_id, qos=1)
        self.client.subscribe(TRAP_TIMEOUT_TOPIC % self.user_id, qos=1)

    def on_disconnect(self, mqtt, userdata, rc):
        print 'disconnected...rc=%s' % str(rc)

    def on_door_topic_message(self, state):
        print 'Ignored message about "door" with state %d' % state

    def on_picture_request_message(self):
        print 'Ignored message about "picture/request"'

    def on_trap_timeout_message(self, time):
        print 'Ignored message about "timeout" with time %d' % time

    def on_message(self, client, data, msg):

        print 'message received...'
        print 'topic: %s, qos: %s, message: %s' % (msg.topic, str(msg.qos), str(msg.payload))

        args = str(msg.payload.decode()).split(' ')

        if msg.topic == (TRAP_DOOR_TOPIC % self.user_id):
            if len(args) != 1:
                print 'Topic "door" accepts just one argument'
                return

            try:
                state = int(args[0])
            except ValueError:
                print 'Topic "door/state" accepts just a number, received "%s"' % args[0]
                return

            self.on_door_topic_message(state)
        elif msg.topic == (TRAP_PICTURE_REQUEST_TOPIC % self.user_id):

            self.on_picture_request_message()
        elif msg.topic == (TRAP_TIMEOUT_TOPIC % self.user_id):
            if len(args) != 1:
                print 'Topic "timeout" accepts just one argument'
                return

            try:
                time = int(args[0])
            except ValueError:
                print 'Topic "timeout" accepts just a number, received "%s"' % args[0]
                return

            self.on_trap_timeout_message(time)
        else:
            print 'Received message for an unknown topic'

    def publish(self, topic, *args):

        if topic == PublishTopic.DOOR_STATE:

            if len(args) != 1:
                print 'Topic "door/state" accepts just one argument'
                return

            try:
                state = int(args[0])
            except ValueError:
                print 'Topic "door/state" accepts just a number, received "%s"' % args[0]
                return

            self.client.publish(TRAP_DOOR_STATE_TOPIC % self.user_id, state, 1, retain=True)
        elif topic == PublishTopic.PICTURE:

            if len(args) != 1:
                print 'Topic "picture" accepts just one argument'
                return

            url = args[0]

            self.client.publish(TRAP_PICTURE_TOPIC % self.user_id, url, 0)
        elif topic == PublishTopic.ALERT:

            if len(args) != 2:
                print 'Topic "alert" accepts two argument'
                return

            url = args[0]

            try:
                time = int(args[1])
            except ValueError:
                print 'Topic "alert" just accepts a number as 2nd argument, received "%s"' % args[1]
                return

            payload = '%s %d' % (url, time)

            self.client.publish(TRAP_ALERT_TOPIC % self.user_id, payload, 0)
        elif topic == PublishTopic.TIMEOUT_ACK:

            if len(args) != 1:
                print 'Topic "timeout/ack" accepts one argument'
                return

            try:
                time = int(args[0])
            except ValueError:
                print 'Topic "timeout/ack" accepts just a number, received "%s"' % args[0]
                return

            self.client.publish(TRAP_TIMEOUT_ACK_TOPIC % self.user_id, time, 1, retain=True)
        else:
            print 'Unknown topic'


class PublishTopic:
    """ Enumeration of topics in which Rpi can publish """
    DOOR_STATE, PICTURE, ALERT, TIMEOUT_ACK = range(4)
