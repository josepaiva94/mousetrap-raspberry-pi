# coding=utf-8

from constants import *
from settings import *

__author__ = 'josepaiva'
__copyright__ = 'Copyright (C) 2017 José Carlos Paiva'
__license__ = 'MIT'
__version__ = '1.0.0'


__all__ = ['constants', 'settings']

